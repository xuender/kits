VERSION = $(shell git describe --tags)
BUILD_TIME = $(shell date +%FT%T)
GIT_COMMIT = $(shell expr substr `git log --pretty=format:'%H' -n 1` 1 7)
PACKAGE = gitee.com/xuender/kits
CONST_PACKAGE = ${PACKAGE}/cmd

lint:
	golangci-lint run
windows:
	# go build -o wach cmd/main.go
	GOOS=windows GOARCH=amd64 go build -ldflags "-H windowsgui" -o watch.exe cmd/kit-watch/main.go
proto:
	protoc --go_out=. pb/*.proto
snappy:
	go build -o kit-snappy cmd/kit-snappy/main.go
