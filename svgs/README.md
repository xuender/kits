# svgs

svg 工具类库

## QrSVG

生成SVG格式的QR码

参考: [goqrsvg](https://github.com/aaronarduino/goqrsvg)，

优化了svg生成方式，压缩了svg尺寸

## Rect

矩形

## Rects

一组矩形，可横纵排序，压缩尺寸

## 依赖

* [svgo](github.com/ajstarks/svgo)
* [barcode](github.com/boombuler/barcode)
