package svgs_test

import (
	"testing"

	"gitee.com/xuender/kits/svgs"
	"github.com/stretchr/testify/assert"
)

func TestRects_Compress(t *testing.T) {
	t.Parallel()

	rect := svgs.NewRects()

	rect.Add(
		&svgs.Rect{X: 0, Y: 0, W: 1, H: 1},
		&svgs.Rect{X: 1, Y: 0, W: 1, H: 1},
		&svgs.Rect{X: 0, Y: 1, W: 1, H: 1},
		&svgs.Rect{X: 1, Y: 1, W: 1, H: 1},
	)

	rs := rect.Compress()
	assert.Equal(t, 1, len(rs), "len")
	assert.Equal(t, 2, rs[0].W, "W")
}
