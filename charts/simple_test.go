package charts_test

import (
	"testing"

	"gitee.com/xuender/kits/charts"
	"github.com/go-playground/assert"
)

func TestSimple_add(t *testing.T) {
	t.Parallel()

	simple := charts.NewSimple("标题", nil)
	key, value := simple.AddOne("my book:-123.4")

	assert.Equal(t, -123.4, value)
	assert.Equal(t, "my book", key)

	key, value = simple.AddOne("礼物:123.4")
	assert.Equal(t, 123.4, value)
	assert.Equal(t, "礼物", key)
}
