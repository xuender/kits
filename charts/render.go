package charts

import (
	"io"

	"github.com/wcharczuk/go-chart"
)

// Render 输出图表.
type Render interface {
	Render(rp chart.RendererProvider, w io.Writer) error
}
