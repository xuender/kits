package gifs

import (
	"image"
	"image/color"
)

// BwQuantizer 黑白双色.
type BwQuantizer struct{}

// Quantize 黑白量化.
func (q *BwQuantizer) Quantize(p color.Palette, m image.Image) color.Palette {
	return color.Palette{color.Black, color.White}
}
