package webs_test

import (
	"fmt"
	"testing"

	"gitee.com/xuender/kits/webs"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func ExampleHandler() {
	handler := []webs.Handler{{
		Method: "GET",
		Path:   "/a",
		File:   "a.txt",
		Type:   "Text",
	}}
	data, _ := yaml.Marshal(handler)
	fmt.Println(string(data))

	// Output:
	// - method: GET
	//   path: /a
	//   file: a.txt
	//   type: Text
}

func TestLoad(t *testing.T) {
	t.Parallel()

	data, err := webs.Load("hs.yaml")
	assert.Equal(t, 2, len(data))
	assert.Nil(t, err)
}
