package jump_test

import (
	"testing"

	"gitee.com/xuender/kits/jump"
)

func TestJump_Jump(t *testing.T) {
	t.Parallel()

	jum := jump.NewJump("/tmp")
	jum.Jump("eis")
}

// func TestJump_History(t *testing.T) {
// 	t.Parallel()

// 	jum := jump.NewJump("/tmp")
// 	jum.History()
// 	jum.Save()

// 	assert.True(t, false)
// }

// func TestJump_Autojump(t *testing.T) {
// 	t.Parallel()
// 	jum := jump.NewJump("/tmp")
// 	jum.Autojump()
// 	jum.Save()

// 	assert.True(t, false)
// }
