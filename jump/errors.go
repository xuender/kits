package jump

import "errors"

var (
	ErrPathExist   = errors.New("path is exist")
	ErrConfigIsDir = errors.New("config is dir")
)
