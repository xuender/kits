package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/go-rod/rod"
)

func main() {
	var (
		cliNum  = 1000
		cliTime int
		cliName string
	)
	// 加载命令行
	flag.IntVar(&cliNum, "n", 1, "截屏次数.")
	flag.IntVar(&cliTime, "t", cliNum, "截屏时间间隔, 单位毫秒.")
	flag.StringVar(&cliName, "s", "s", "截屏文件名.")
	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatal("缺少网址参数")
	}

	url := flag.Arg(0)
	log.Printf("抓取: %s", url)
	page := rod.New().MustConnect().MustPage(url)
	page.MustWaitLoad()

	for i := 0; i < cliNum; i++ {
		name := fmt.Sprintf("%s%03d.png", cliName, i)
		page.MustScreenshot(name)
		log.Printf("保存: %s", name)
		time.Sleep(time.Millisecond * time.Duration(cliTime))
	}
}
