package main

import (
	_ "embed"
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"gitee.com/xuender/kits/cmd"
	"gitee.com/xuender/kits/jump"
	"gitee.com/xuender/oils/base"
	"gitee.com/xuender/oils/logs"
)

//go:embed kit-jump.zsh
var jumpBytes []byte

func main() {
	var (
		shell bool
		list  bool
	)

	flag.Usage = usage
	flag.BoolVar(&shell, "s", false, "shell")
	flag.BoolVar(&list, "l", false, "list paths")
	flag.Parse()

	if shell {
		home := base.Panic1(os.UserHomeDir())
		file := base.Panic1(os.Create(filepath.Join(home, "kit-jump.zsh")))

		defer file.Close()

		_, _ = file.Write(jumpBytes)

		return
	}

	if cmd.ModVersion != "" {
		logs.SetInfoLevel()
	}

	jum := jump.NewJump("~/.local/share/kit-jump")

	if list {
		jum.List()

		return
	}

	if len(flag.Args()) == 0 {
		jum.Output("")

		return
	}

	jum.Output(flag.Arg(0))
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), "kit-jump [%s]\n\n", cmd.ModVersion)
	fmt.Fprintf(flag.CommandLine.Output(), "参考 autojump, 目录跳转工具\n\n")
	fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Fprintf(flag.CommandLine.Output(), "\n模块: %s\n提交: %s\n", cmd.ModPath, cmd.ModSum)
}
