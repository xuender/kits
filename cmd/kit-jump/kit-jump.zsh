f() {
    if [[ ${1} == -* ]] && [[ ${1} != "--" ]]; then
        kit-jump ${@}
        return
    fi

    setopt localoptions noautonamedirs
    local output="$(kit-jump ${@})"
    if [[ -d "${output}" ]]; then
        if [ -t 1 ]; then  # if stdout is a terminal, use colors
                echo -e "\\033[31m${output}\\033[0m"
        else
                echo -e "${output}"
        fi
        cd "${output}"
    else
        echo "kit-jump: directory '${@}' not found"
        echo "\n${output}\n"
        echo "Try \`kit-jump --help\` for more information."
        false
    fi
}
