package main

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitee.com/xuender/oils/base"
	"gitee.com/xuender/oils/execs"
	"gitee.com/xuender/oils/unit"
)

func main() {
	dir := "."
	if len(os.Args) > 0 {
		dir = os.Args[1]
	}

	base.Panic(filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		ext := filepath.Ext(path)
		if strings.ToUpper(ext) != ".ZIP" {
			return nil
		}

		if Check(path) {
			_, _ = io.WriteString(os.Stdout, fmt.Sprintf("\"%s\" %s, %d", path, unit.ByteUnit(info.Size()), info.Size()))
		}

		return nil
	}))
}

func Check(path string) bool {
	err := execs.Exec("unzip", "-v", path)

	return err == nil
}
