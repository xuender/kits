# kits

工具箱

## 全部安装

```shell
go install gitee.com/xuender/kits/cmd/...@latest
```

## kit-web

简单的web服务，发布静态文件资源

可以设置每次访问的休眠时间，用于测试

### 安装

```shell
go install gitee.com/xuender/kits/cmd/kit-web@latest
```

### 选项

* -p 端口号
* -w 静态资源目录
* -s 每次请求休眠时间, 单位毫秒
* -d 显示调试信息
* -g gzip压缩输出

## kit-qr

根据标准输入或命令行参数生成二维码

### 安装

```shell
go install gitee.com/xuender/kits/cmd/kit-qr@latest
```

### 选项

* -o 生成的QR码文件
* -t 生成的QR码文件格式，png、svg、gif、jpeg
* -d 显示调试信息
* -w QR码宽度，svg格式忽略
* -b QR码线条宽度,仅限svg格式使用

## kit-chart

根据命令行输入生成简单图表

```shell
go install gitee.com/xuender/kits/cmd/kit-chart@latest
```

### 选项

* -o 生成图表
* -n 图表名称
* -t 图表文件格式
* -c 图表标题
* -s 图表式样

## kit-watch

女儿老玩游戏，做个监视屏幕的服务

```shell
go install gitee.com/xuender/kits/cmd/kit-watch@latest
```

## kit-shell

拼多多云环境需要个`main`可执行文件，但需要运行的程序未必是这样，需要个壳程序进行调用

```shell
go install gitee.com/xuender/kits/cmd/kit-shell@latest
```

配置文件 shell.toml
```toml
command = "./Yearning"
args = ["run", "-p", "port", "-s", "xxxxxxxxxx"]
[env]
port = "SERVER_PORT"
```

env 部分可以使用环境变量进行替换

### 选项

* -a 访问密码
* -p 端口号
* -d 显示调试信息

## svgs

[svg](svgs/) 工具

## gifs

[gif](gifs/) 工具

## charts

[图表](charts/) 工具
